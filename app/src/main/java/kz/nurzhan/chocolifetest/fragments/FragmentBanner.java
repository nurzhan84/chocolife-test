package kz.nurzhan.chocolifetest.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;

import kz.nurzhan.chocolifetest.R;

/**
 * Created by nurzhan on 9/15/2016.
 */
public class FragmentBanner extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_banner, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView3);
        Bundle bundle = getArguments();
        String imageUrl = bundle.getString("imageUrl");
        Glide.with(getActivity()).load(imageUrl).into(imageView);

        return view;
    }

}
