package kz.nurzhan.chocolifetest.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kz.nurzhan.chocolifetest.R;
import kz.nurzhan.chocolifetest.utils.Logger;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentFeatures extends Fragment {


    public FragmentFeatures() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragmet_features, container, false);

        TextView textView = (TextView) view.findViewById(R.id.textView11);

        Bundle bundle = getArguments();
        String txt = bundle.getString("features");

        Spanned result;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(txt,Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(txt);
        }
        textView.setText(result);

        return view;
    }

}
