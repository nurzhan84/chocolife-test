package kz.nurzhan.chocolifetest.fragments;


import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import kz.nurzhan.chocolifetest.R;
import kz.nurzhan.chocolifetest.utils.Logger;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentConditions extends Fragment {


    public FragmentConditions() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fragment_conditions, container, false);

        TextView textView = (TextView) view.findViewById(R.id.textView10);
        Bundle bundle = getArguments();
        ArrayList<String> txt = bundle.getStringArrayList("terms");
        String str="";
        if(txt!=null){
            for(int i =0; i<txt.size(); i++){
                Spanned result;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    result = Html.fromHtml(txt.get(i),Html.FROM_HTML_MODE_LEGACY);
                } else {
                    result = Html.fromHtml(txt.get(i));
                }
                str = str+ result+"\n";
            }

            textView.setText(str);

        }

        return view;
    }

}
