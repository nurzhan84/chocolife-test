package kz.nurzhan.chocolifetest.activities;


import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.viewpagerindicator.CirclePageIndicator;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import kz.nurzhan.chocolifetest.R;
import kz.nurzhan.chocolifetest.fragments.FragmentBanner;
import kz.nurzhan.chocolifetest.fragments.FragmentConditions;
import kz.nurzhan.chocolifetest.fragments.FragmentFeatures;
import kz.nurzhan.chocolifetest.utils.Logger;
import kz.nurzhan.chocolifetest.utils.WrapContentViewPager;

public class ItemDetailsActivity extends Basic {

    //стандартный viewPager для баннерной карусели
    ViewPager viewPagerBanner;
    //модифицированный ViewPager. Позволяет исполтзовать его в ScrollView, задавая необходимую высоту в соответсыии с контентом
    WrapContentViewPager viewPagerPages;

    TabLayout tabLayout;
    CirclePageIndicator circleIndicator;
    TextView title_tv, price1_tv,price2_tv;
    ScrollView scrollView;

    ArrayList<String> bannerImages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null){
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        viewPagerBanner = (ViewPager)findViewById(R.id.viewpager1);
        viewPagerPages= (WrapContentViewPager)findViewById(R.id.viewpager2);
        scrollView = (ScrollView) findViewById(R.id.scrollView);
        scrollView.setVisibility(View.INVISIBLE);
        tabLayout = (TabLayout) findViewById(R.id.tabs);


        title_tv = (TextView) findViewById(R.id.textView7);
        price1_tv = (TextView) findViewById(R.id.textView8);
        price2_tv = (TextView) findViewById(R.id.textView9);

        getItemTask = new GetItemRequest().execute();

        viewPagerPages.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }
            @Override
            public void onPageSelected(int position) {
                viewPagerPages.reMeasureCurrentPage(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupBannerViewPager(ViewPager viewPager,int imageCount, ArrayList<String> imageUrl) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for(int i=0; i<imageCount; i++){
            Fragment fragment = new FragmentBanner();
            Bundle bundle = new Bundle();
            bundle.putString("imageUrl",imageUrl.get(i));
            fragment.setArguments(bundle);
            adapter.addFragment(fragment, "");
        }
        viewPager.setAdapter(adapter);
        circleIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        circleIndicator.setViewPager(viewPagerBanner);
    }

    private void setupViewPager(ViewPager viewPager, ArrayList<String> arg1,String arg2) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        Fragment f1 = new FragmentConditions();
        Bundle b1 = new Bundle();
        b1.putStringArrayList("terms",arg1);
        f1.setArguments(b1);
        adapter.addFragment(f1, getString(R.string.conditions));

        Fragment f2 = new FragmentFeatures();
        Bundle b2 = new Bundle();
        b2.putString("features",arg2);
        f2.setArguments(b2);
        adapter.addFragment(f2, getString(R.string.features));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPagerPages);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //отменяем работу AsynсTask, при закрытии активити, если он еще выполняется
        if(getItemTask!=null){getItemTask.cancel(true);}
    }

    // AsyncTask для загрузки данных
    //--------------------------------------get items-----------------------------------------------
    AsyncTask<Void, Void, String> getItemTask;
    private class GetItemRequest extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute(){
            showWait();
        }
        @Override
        protected String doInBackground(Void... params) {return getItem();
        }

        protected void onPostExecute(String results) {
            hideWait();

            //если связи с сервером нет, то пытаемся взять данные из кэша
            if(results==null){
                results = prefs.getString("itemDetails",null);
            }
            if (results!=null) {
                prefs.edit().putString("itemDetails",results).apply();
                try {
                    JSONObject jsonObject = new JSONObject(results);
                    JSONObject result = jsonObject.getJSONObject("result");
                    JSONArray images = result.getJSONArray("images");
                    for(int i=0; i<images.length();i++){
                        bannerImages.add((String) images.get(i));
                    }
                    title_tv.setText(result.getString("title"));
                    price1_tv.setText(result.getString("full_price"));
                    price1_tv.setPaintFlags(price1_tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    price2_tv.setText(result.getString("price"));


                    JSONArray term = result.getJSONArray("terms");
                    ArrayList<String> arg1= new ArrayList<>();
                    for(int i=0; i<term.length();i++){
                        arg1.add((String) term.get(i));
                    }
                    String arg2 = result.getString("features");

                    setupViewPager(viewPagerPages , arg1, arg2);
                    setupBannerViewPager(viewPagerBanner,images.length(),bannerImages);
                    scrollView.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage(e.getMessage(),null);
                }

            }else {
                //если связи с сервером нет и кэш пустой, то показываем сообщение
                showConnectionErrorMessage(getString(R.string.connection_error), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getItemTask = new GetItemRequest().execute();
                    }
                });
            }

        }
    }

    String getItem () {
        URL url = null;
        try {
            url = new URL("https://chocolife.me/static/test/mobile/deal_31272.json");
            Logger.i("Request: " + url.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");

            String response;
            try {
                int code = conn.getResponseCode();
                Logger.i("Response Code: " + code+" "+conn.getResponseMessage());

                InputStream in;
                if(code<400){
                    in = new BufferedInputStream(conn.getInputStream());
                }else{
                    in = new BufferedInputStream(conn.getErrorStream());
                }
                response = IOUtils.toString(in, "UTF-8");
                Logger.i("Response: "+response);
            }
            finally {
                conn.disconnect();
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------


}
