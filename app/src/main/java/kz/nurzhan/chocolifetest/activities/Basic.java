package kz.nurzhan.chocolifetest.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import kz.nurzhan.chocolifetest.R;
import kz.nurzhan.chocolifetest.dialogs.WaitDialog;

/**
 * Created by nurzhan on 9/15/2016.
 *
 * В этом классе содержатся часто используемые методы
 */


public class Basic extends AppCompatActivity {

    SharedPreferences prefs;
    WaitDialog waitDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        waitDialog = new WaitDialog();
        waitDialog.setCancelable(false);
        prefs = getSharedPreferences("chocolife", Context.MODE_PRIVATE);
    }

    public void showWait(){
        waitDialog.show(getSupportFragmentManager(),"wait dialog");
    }
    public void  hideWait(){
        if(waitDialog!=null){  waitDialog.dismiss();}
    }


    public void showMessage(String message, DialogInterface.OnDismissListener onDismissListener){
        try{
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            alertDialog.setOnDismissListener(onDismissListener);
        }catch (Exception e){}
    }


    public void showConnectionErrorMessage(String message, final DialogInterface.OnDismissListener onDismissListener, final DialogInterface.OnClickListener tryAgain){
        try{
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setCancelable(false)
                    .setMessage(message)
                    .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if(onDismissListener!=null){
                                onDismissListener.onDismiss(dialog);
                            }
                        }
                    })
                    .setNegativeButton(getString(R.string.try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (tryAgain!=null) {
                                tryAgain.onClick(dialogInterface,i);
                            }
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();

        }catch (Exception e){}

    }
}
