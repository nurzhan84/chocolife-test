package kz.nurzhan.chocolifetest.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import kz.nurzhan.chocolifetest.R;
import kz.nurzhan.chocolifetest.utils.Logger;

public class MainActivity extends Basic {

    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    Adapter adapter;

    ArrayList<String> itemTitle = new ArrayList<>();
    ArrayList<String> itemPrice1 = new ArrayList<>();
    ArrayList<String> itemPrice2 = new ArrayList<>();
    ArrayList<String> itemImageUrl = new ArrayList<>();
    ArrayList<String> itemDiscount = new ArrayList<>();
    ArrayList<String> itemBought = new ArrayList<>();
    ArrayList<ArrayList<String>> itemData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        itemData.add(itemTitle);
        itemData.add(itemPrice1);
        itemData.add(itemPrice2);
        itemData.add(itemImageUrl);
        itemData.add(itemDiscount);
        itemData.add(itemBought);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new Adapter(this,itemData);
        recyclerView.setAdapter(adapter);

        if(itemTitle.size()==0){
            getItemsTask = new GetItemsRequest().execute();
        }

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getItemsTask = new GetItemsRequest().execute();
            }
        });
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        //отменяем работу AsynсTask, при закрытии активити, если он еще выполняется
        if(getItemsTask!=null){getItemsTask.cancel(true);}
    }


    //----------------------------------Adapter для RecyclerView------------------------------------
    private class Adapter extends RecyclerView.Adapter<Holder>{

        private Context context;
        private ArrayList<ArrayList<String>> item_data;

        public Adapter(Context c ,ArrayList<ArrayList<String>> itemList) {
            context=c;
            item_data = itemList;
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_main_list, parent, false);
            return new Holder(view);
        }

        @Override
        public void onBindViewHolder(Holder viewHolder, int position) {
            viewHolder.title.setText(item_data.get(0).get(position));
            viewHolder.price1.setText(item_data.get(1).get(position));
            viewHolder.price1.setPaintFlags(viewHolder.price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            viewHolder.price2.setText(item_data.get(2).get(position));
            Glide.with(context).load(itemData.get(3).get(position)).into(viewHolder.imageView);
            viewHolder.discount.setText("-"+item_data.get(4).get(position)+"%");
            viewHolder.bought.setText(item_data.get(5).get(position));
        }

        @Override
        public int getItemCount() {
            return item_data.get(0).size();
        }
    }
    class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView title,price1,price2,discount,bought;
        ImageView imageView;

        public Holder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.textView);
            price1 = (TextView) itemView.findViewById(R.id.textView2);
            price2 = (TextView) itemView.findViewById(R.id.textView3);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            discount= (TextView) itemView.findViewById(R.id.textView4);
            bought= (TextView) itemView.findViewById(R.id.textView5);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //тут обрабатывается нажатие на любой элемент из списка
            //int pos = getAdapterPosition();
            startActivity(new Intent(MainActivity.this,ItemDetailsActivity.class));
        }
    }
    //----------------------------------------------------------------------------------------------

    // AsyncTask для загрузки данных в список
    //--------------------------------------get items-----------------------------------------------
    AsyncTask<Void, Void, String> getItemsTask;
    private class GetItemsRequest extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute(){
            swipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    swipeRefreshLayout.setRefreshing(true);
                }
            });
        }
        @Override
        protected String doInBackground(Void... params) {return getItems();
        }

        protected void onPostExecute(String results) {
            swipeRefreshLayout.setRefreshing(false);

            //если связи с сервером нет, то пытаемся взять данные из кэша
            if(results==null){
                results = prefs.getString("items_list",null);
            }

            if (results!=null) {
                prefs.edit().putString("items_list",results).apply();

                itemTitle.clear();
                itemPrice1.clear();
                itemPrice2.clear();
                itemImageUrl.clear();
                itemDiscount.clear();
                itemBought.clear();

                try {
                    JSONObject jsonObject = new JSONObject(results);
                    JSONArray result = jsonObject.getJSONArray("result");
                    for(int i=0; i<result.length();i++){
                        String title = result.getJSONObject(i).getString("title");
                        itemTitle.add(title);
                        String price1 = result.getJSONObject(i).getString("full_price");
                        itemPrice1.add(price1);
                        String price2 = result.getJSONObject(i).getString("price");
                        itemPrice2.add(price2);
                        String imgUrl = result.getJSONObject(i).getString("image_url");
                        itemImageUrl.add(imgUrl);
                        String discount = result.getJSONObject(i).getString("discount");
                        itemDiscount.add(discount);
                        String bought = result.getJSONObject(i).getString("bought");
                        itemBought.add(bought);
                    }

                    adapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    showMessage(e.getMessage(),null);
                }

            }else {
                //если связи с сервером нет и кэш пустой, то показываем сообщение
                showConnectionErrorMessage(getString(R.string.connection_error), null, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        getItemsTask = new GetItemsRequest().execute();
                    }
                });
            }

        }
    }

    String getItems () {
        URL url = null;
        try {
            url = new URL("https://chocolife.me/static/test/mobile/deals.json");
            Logger.i("Request: " + url.toString());

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");

            String response;
            try {
                int code = conn.getResponseCode();
                Logger.i("Response Code: " + code+" "+conn.getResponseMessage());

                InputStream in;
                if(code<400){
                    in = new BufferedInputStream(conn.getInputStream());
                }else{
                    in = new BufferedInputStream(conn.getErrorStream());
                }
                response = IOUtils.toString(in, "UTF-8");
                Logger.i("Response: "+response);
            }
            finally {
                conn.disconnect();
            }
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
    //----------------------------------------------------------------------------------------------
}
